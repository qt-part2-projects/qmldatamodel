import QtQuick 2.0

Rectangle {
    width: 240
    height: 300
    color: "#cac4c4"

    GridView {
        id: view
        anchors.fill: parent
        anchors.margins: 20

        clip: true

        model: 100

        cellWidth: 45
        cellHeight: 45

        delegate: numberDelegate

        //flow : GridView.TopToBottom
    }

    Component {
        id: numberDelegate

        Rectangle {
            width: 40
            height: 40
            color: "#26e653"
            Text {text: index; anchors.centerIn: parent}
        }
    }
}
