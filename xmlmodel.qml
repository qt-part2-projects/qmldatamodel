import QtQuick 2.0

import QtQuick.XmlListModel 2.0

Rectangle {
    width: 300
    height: 480
    color: "#e2d9d9"

    Component {
        id: imageDelegate

        Rectangle {
            width: listView.width
            height: titleId.implicitHeight + descrId.implicitHeight
            color: '#333'

            Column {
                Text {
                    id: titleId
                    text: title
                    color: '#e0e0e0'
                    font.pixelSize: 18
                    wrapMode: Text.WordWrap
                    width: listView.width
                }
                Text {
                    id: descrId
                    text: description
                    color: '#e0e0e0'
                    font.pixelSize: 14
                    wrapMode: Text.WordWrap
                    width: listView.width
                }
//                Image {
//                    width: listView.width
//                    height: 200
//                    fillMode: Image.PreserveAspectCrop
//                    source: imageSource
//                }
            }
        }
    }

    XmlListModel {
        id: imageModel

        source: "http://rss.cnn.com/rss/edition_travel.rss"//"http://feeds.nationalgeographic.com/ng/photography/photo-of-the-day/"
        query: "//item"///rss/channel/item"

        XmlRole { name: "title"; query: "title/string()" }
        //XmlRole { name: "imageSource"; query: "substring-before(substring-after(description/string(), 'img src=\"'), '\"')" }
        XmlRole{ name: "description"; query: "description/string()"}
    }

    ListView {
        id: listView
        anchors.fill: parent
        model: imageModel
        delegate: imageDelegate
        spacing: 20
    }
}
