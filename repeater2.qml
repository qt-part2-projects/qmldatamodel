import QtQuick 2.0

Column {
    spacing: 2

    Repeater {
        model: ["Enterprise", "Columbia", "Challenger", "Discovery", "Endeavour", "Atlantis"]

        Rectangle {
            width: 120
            height: 32
            radius: 3
            color: "#8484d7"
            Text{
                anchors.centerIn: parent
                text: modelData + ' (' + index + ')'
            }
        }
    }
}
