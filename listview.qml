import QtQuick 2.5
import QtQuick.Window 2.14

Window{
//    width: 80 //1
//    height: 300 //1
    visible: true
    width: 480 //2
    height: 80 //2
    title: qsTr("Listview")


    ListView {
        anchors.fill: parent
        anchors.margins: 20

        clip: true //1

        model: 100
        orientation: ListView.Horizontal//2
        delegate: numberDelegate
        spacing: 5
    }

    Component {
        id: numberDelegate

        Rectangle {
            width: 40
            height: 40
            color: "#5ed975"
            Text{text: index; anchors.centerIn: parent}
        }
    }
}
