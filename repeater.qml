import QtQuick 2.0

Column {
    spacing: 2

    Repeater {
        model: 10
        Rectangle {
            width: 120
            height: 32
            color: "#8484d7"
            Text{
                anchors.centerIn: parent
                text: index
            }
        }
    }
}
