import QtQuick 2.0

Rectangle {
    id: root
    width: 240
    height: 400
    color: "#cac4c4"

    PathView {
        anchors.fill: parent

        delegate: flipCardDelegate
        model: 100

        path: Path {
            // начало пути - вверху по центру
            startX: root.width/2
            startY: 0
            // атрибуты точки начала пути
            PathAttribute { name: "itemZ"; value: 0 }
            PathAttribute { name: "itemAngle"; value: -90.0; }
            PathAttribute { name: "itemScale"; value: 0.5; }
            // вертикальная линия к 40% от высоты
            PathLine { x: root.width/2; y: root.height*0.4; }
            PathPercent { value: 0.48; }
            // вертикальная линия к 50% от высоты (центр окна)
            PathLine { x: root.width/2; y: root.height*0.5; }
            PathAttribute { name: "itemAngle"; value: 0.0; }
            PathAttribute { name: "itemScale"; value: 1.0; }
            PathAttribute { name: "itemZ"; value: 100 }
            // вертикальная линия к 60% от высоты
            PathLine { x: root.width/2; y: root.height*0.6; }
            PathPercent { value: 0.52; }
            // вертикальная линия вниз по центру
            PathLine { x: root.width/2; y: root.height; }
            PathAttribute { name: "itemAngle"; value: 90.0; }
            PathAttribute { name: "itemScale"; value: 0.5; }
            PathAttribute { name: "itemZ"; value: 0 }
        }

        pathItemCount: 16

        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
    }
    // делегат отображения элемента списка
    Component {
        id: flipCardDelegate
        Rectangle {
            id: wrapper
            width: 64
            height: 64
            antialiasing: true
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#2ed5fa" }
                GradientStop { position: 1.0; color: "#2467ec" }
            }
            visible: PathView.onPath
            scale: PathView.itemScale // из атрибута пути
            z: PathView.itemZ // из атрибута пути
            property variant rotX: PathView.itemAngle // из атрибута пути
            transform: Rotation {
                axis { x: 1; y: 0; z: 0 }
                angle: wrapper.rotX;
                origin { x: 32; y: 32; }
            }
            Text{text: index; anchors.centerIn: parent}
        }
    }
}

