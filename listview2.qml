//Keyboard Navigation and Highlighting
import QtQuick 2.5
import QtQuick.Window 2.14

Window{
//    width: 80 //1
//    height: 300 //1
    visible: true

    title: qsTr("Listview")
    width: 240
    height: 300

    ListView {
        id: view
        anchors.fill: parent
        anchors.margins: 20

        clip: true

        model: 10

        delegate: numberDelegate
        spacing: 5

        highlight: highlightComponent
        focus: true

        header: headerComponent
        footer: footerComponent
        //highlightRangeMode: ListView.StrictlyEnforceRange //ApplyRange  //StrictlyEnforceRange //NoHighlightRange
        //highlightFollowCurrentItem : true
    }

    Component {
        id: highlightComponent

        Rectangle {
            width: ListView.view.width
            color: "#75c26b"
        }
    }
    Component {
            id: highlightComponent2

            Item {
                width: ListView.view.width
                height: ListView.view.currentItem.height

                y: ListView.view.currentItem.y

//                Behavior on y {
//                    SequentialAnimation {
//                        PropertyAnimation { target: highlightRectangle; property: "opacity"; to: 0; duration: 1200 }
//                        NumberAnimation { duration: 1 }
//                        PropertyAnimation { target: highlightRectangle; property: "opacity"; to: 1; duration: 1200 }
//                    }
//                }

                Rectangle {
                    id: highlightRectangle
                    anchors.fill: parent
                color: "#75c26b"
                }


            }
        }
    Component {
        id: headerComponent

        Rectangle {
            width: ListView.view.width
            height: 20
            color: "#e3f32b"
            Text {text: 'Header'; anchors.centerIn: parent}

        }
    }

    Component {
        id: footerComponent

        Rectangle {
            width: ListView.view.width
            height: 20
            color: "#e3f32b"
            Text {text: 'Footer'; anchors.centerIn: parent}
        }
    }
    Component {
        id: numberDelegate

        Item {
            width: ListView.view.width
            height: 40

            Text {
                anchors.centerIn: parent

                font.pixelSize: 10

                text: index
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    view.currentIndex = index
                }
            }
        }

    }
}
